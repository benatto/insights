from flask import json

from app import app

app.testing = True
app.debug = True


def test_if_returns_empty_when_user_does_not_exist():
    response = app.test_client().get("/api/v1/insights/users/1?q=hotels")
    response_json = json.loads(response.get_data(as_text=True))

    assert not response_json

    assert response.status_code == 200


def test_if_returns_amenities_with_valid_user():
    response = app.test_client().get("/api/v1/insights/users/33?q=amenities")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["amenities"]) == 3

    assert response.status_code == 200


def test_if_returns_one_amenity_with_top_filter():
    response = app.test_client().get("/api/v1/insights/users/33?top=1&q=amenities")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["amenities"]) == 1


def test_if_returns_only_amenities():
    response = app.test_client().get("/api/v1/insights/users/33?q=amenities")
    response_json = json.loads(response.get_data(as_text=True))

    assert response_json.get("hotels") is None
    assert len(response_json["amenities"]) == 3


def test_if_returns_only_hotels():
    response = app.test_client().get("/api/v1/insights/users/33?q=hotels")
    response_json = json.loads(response.get_data(as_text=True))

    assert response_json.get("amenities") is None
    assert len(response_json["hotels"]) == 1


def test_if_returns_400_with_qs_not_supported():
    response = app.test_client().get("/api/v1/insights/users/33?q=test")

    assert response.status_code == 400


def test_if_returns_400_with_top_not_supported():
    response = app.test_client().get("/api/v1/insights/users/33?top=s&q=amenities")

    assert response.status_code == 400


def test_if_returns_400_without_query_search():
    response = app.test_client().get("/api/v1/insights/users/33")

    assert response.status_code == 400


def test_if_returns_400_with_top_with_negative_number():
    response = app.test_client().get("/api/v1/insights/users/33?q=amenities&top=-1")

    assert response.status_code == 400


def test_if_returns_only_one_insight_with_valid_top_parameter():
    response = app.test_client().get("/api/v1/insights/users/33?q=amenities&top=1")
    response_json = json.loads(response.get_data(as_text=True))

    assert len(response_json["amenities"]) == 1
