import os

from db import db

from flask import Flask
from flask_restful import Api
from resources.insight import Insight


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object(config)
    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)

    return app


app = create_app()

api = Api(app)
api.add_resource(Insight, "/api/v1/insights/users/<int:user_id>")


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
