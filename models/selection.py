from db import db
from sqlalchemy import desc


class SelectionsModel(db.Model):
    """This class contains what amenities a particular user has selected in the past.
    The file has three columns; timestamp(int), user_id(int), amenity_id(int).
    """

    __tablename__ = "selections"

    id = db.Column(db.Integer, primary_key=True)
    amenity_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    timestamp = db.Column(db.Integer)
    quantity = db.Column(db.Integer)

    def json(self):
        return {
            "amenity_id": int(self.amenity_id),
            "timestamp": int(self.timestamp),
            "quantity": int(self.quantity),
        }

    @classmethod
    def find_top_insights_by_user_id(cls, user_id, top):
        return (
            cls.query.filter_by(user_id=user_id)
            .order_by(desc(SelectionsModel.quantity))
            .limit(top)
            .all()
        )
