from db import db
from sqlalchemy import desc


class ClicksModel(db.Model):
    """This file contains what hotels the user has clicked on in the past.
    The file has four columns; timestamp(int), user_id(int), hotel_id(int),
    hotel_region(string).
    """

    __tablename__ = "clicks"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    hotel_id = db.Column(db.Integer)
    timestamp = db.Column(db.Integer)
    hotel_region = db.Column(db.String(255))
    quantity = db.Column(db.Integer)

    def json(self):
        return {
            "hotel_id": int(self.hotel_id),
            "timestamp": int(self.timestamp),
            "hotel_region": self.hotel_region,
            "quantity": int(self.quantity),
        }

    @classmethod
    def find_top_insights_by_user_id(cls, user_id, top):
        return (
            cls.query.filter_by(user_id=user_id)
            .order_by(desc(ClicksModel.quantity))
            .limit(top)
            .all()
        )
