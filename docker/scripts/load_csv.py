#!/usr/bin/python3

# Helper script to load data from CSV to Postgres

import psycopg2
import os
import csv


DB_USER = os.getenv("DB_USER")
DB_HOST = os.getenv("DB_HOST")
DB_NAME = os.getenv("DB_NAME")
DB_PORT = os.getenv("DB_PORT")
DB_PASSWORD = os.getenv("DB_PASSWORD")

SELECTION_CSV_FILE = "data/csv/selections.csv"
CLICK_CSV_FILE = "data/csv/clicks.csv"

SELECTION_QUERY = """
        INSERT INTO selections (amenity_id, user_id, quantity, timestamp)
        VALUES (%s, %s, %s, %s)
        ON CONFLICT (amenity_id, user_id)
        DO UPDATE SET quantity = selections.quantity + 1;
    """

CLICK_QUERY = """
        INSERT INTO clicks (user_id, hotel_id, hotel_region, quantity, timestamp)
        VALUES (%s, %s, %s, %s, %s)
        ON CONFLICT (hotel_id, user_id)
        DO UPDATE SET quantity = clicks.quantity + 1;
    """


def get_params_selection(row):
    return row[0], row[1], row[2], 1


def get_params_click(row):
    # handling hotel_region with comma in the name
    hotel_region = ", ".join(row[3:])
    return row[0], row[1], row[2], hotel_region, 1


def execute_query_sql(conn, row, _type):

    cursor = conn.cursor()

    if _type == "selection":
        timestamp, user_id, amenity_id, quantity = get_params_selection(row)
        cursor.execute(SELECTION_QUERY, (amenity_id, user_id, quantity, timestamp))
    elif _type == "click":
        timestamp, user_id, hotel_id, hotel_region, quantity = get_params_click(row)
        cursor.execute(
            CLICK_QUERY, (user_id, hotel_id, hotel_region, quantity, timestamp)
        )
    else:
        print(f"Type not supported: {_type}")
        return
    conn.commit()


def save_csv_to_db(filename, conn, _type):

    with open(filename) as csv_file:
        csv_data = csv.reader(csv_file, delimiter=",")
        for row in csv_data:
            execute_query_sql(conn, row, _type)


def connect_database(user, host, port, db_name, password):

    connection = psycopg2.connect(
        user=user, host=host, port=port, database=db_name, password=password
    )

    return connection


if __name__ == "__main__":

    try:
        conn = connect_database(DB_USER, DB_HOST, DB_PORT, DB_NAME, DB_PASSWORD)
        save_csv_to_db(CLICK_CSV_FILE, conn, "click")
        save_csv_to_db(SELECTION_CSV_FILE, conn, "selection")
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
    finally:
        if conn:
            conn.close()
