# Insights

User insights system.

## Table of Contents

* [How to build the service](#introduction)
* [How to run tests](#tests)
* [Example using API Endpoint](#example)
* [Questions](#questions)
  * [Question 1](#question1)
  * [Question 2](#question2)
  * [Question 3](#question3)
  * [Question 4](#question4)
* [Bonus Questions](#bonusquestions)
  * [Bonus Question 1](#bonusquestion1)
  * [Bonus Question 2](#bonusquestion2)
  * [Bonus Question 3](#bonusquestion3)

## How to build the service <a name="build"></a>

```
$ docker-compose up
```

and load the CSV file to database with:

```
$ docker-compose exec insights python docker/scripts/load_csv.py
```

# How to run tests <a name="tests"></a>

You should be able to run unit tests with docker.

```
$ docker-compose -f docker-compose.testing.yml run tests docker/scripts/tests
```

```
Name                     Stmts   Miss  Cover   Missing
------------------------------------------------------
models/click.py             14      0   100%
models/selection.py         13      0   100%
resources/constants.py       3      0   100%
resources/insight.py        30      0   100%
resources/utils.py          16      0   100%
------------------------------------------------------
TOTAL                       76      0   100%
```

# Example using API Endpoint <a name="example"></a>

Some notes about the API behaviour:

* The API user **HAS TO** provide a valid query string parameter "q", otherwise the API
  will return a bad request. So the user has to be explicity.
* Endpoint: `http://localhost:5000/api/v1/insights/users/<user_id>?q=<insights>`
* Query String Parameters:
  * `q (required)`: insight filtering (amenities, hotels, ...)
  * `top (optional)`: top-N insights. It will filter all insights with the same top,
     but it would be possible to add one for each insight.

Example of API response:

http://localhost:5000/api/v1/insights/users/4482986700820473597?top=2&q=amenities,hotels

```
{
    "amenities": [
        {
            "amenity_id": 8177202,
            "timestamp": 1547229476,
            "quantity": 3120
        },
        {
            "amenity_id": 4681403,
            "timestamp": 1547229477,
            "quantity": 690
        }
    ],
    "hotels": [
        {
            "hotel_id": 107478,
            "timestamp": 1547200876,
            "hotel_region": "North Holland",
            "quantity": 18
        },
        {
            "hotel_id": 42161,
            "timestamp": 1547144899,
            "hotel_region": "North Holland",
            "quantity": 18
        }
    ]
}
```

http://localhost:5000/api/v1/insights/users/4482986700820473597

```
{
    "Message": "Invalid request. Please provide a valid query string"
}
```

http://localhost:5000/api/v1/insights/users/4482986700820473597?top=1

```
{
    "Message": "Invalid request. Please provide a valid query string"
}
```

## Questions <a name="questions"></a>

### What are the assumptions that you made during the implementation? <a name="question1"></a>

```
I have assumed the data will always be available and it can come from different platforms.
I have created only one endpoint, so if I would like to scale with more insights, I only
have to call only one endpoint and just add new filters.

I have created a filter called “top” to control the top-N amenities/hotels and I have created
another one called “q” to return the insights. 

I've loaded the CSV to a database and added a new column called “quantity”. I've added
constraints to my database to update the quantity column based on “amenity_id” and “user_id”
(upsert).

If the user does not exist I'm not returning 404, I'm returning empty data for this specific
case. The same applies for a valid user without insights. The reason is simple: it is an insight
API, not user API.

```

### What are the performance characteristics of your implementation? <a name="question2"></a>

```
I've added a new column called “quantity” to both tables (selections, clicks) to store the
number of times one specific user chooses one specific amenity and the same for hotels.

I'm loading the CSV data to the  database and I believe this approach is more efficient than
using CSV file in memory.
```

### If you could load test it, what do you expect to see in the result? <a name="question3"></a>

```
I expect that the postgres will be a bottleneck for a huge amount of data. It would be
necessary a database solution to scale horizontally and in front of the API add a loadbalancer.
```

### If you had more time, how would you improve your solution? <a name="question4"></a>

```
It would be nice to create a service to produce the data using event-driven programming where
would be possible to publish messages to topics/queues and one or more different services could
read and process them.

The benefit of this new approach would be scalability. It would also be advantageous to deploy
using localstack because it provides the same functionality and APIs as the real AWS cloud
environment. 

With the same purpose in mind we could use skaffold to deploy in a local kubernetes.
In addition with more time I would add behavioural test and API documentation (probably apiary).

I never worked with, but we could use spark to aggregate real-time events (insights).
```

## Bonus Questions <a name="bonusquestions"></a>

### What other user insights could we possibly generate from this data? <a name="bonusquestion1"></a>

```
Based on timestamp the following insights could also be generated:

* Time of the year the user in general travels the most

* Type of preferred amenities based on the time of the year

* Where the user tends to travel depending on the time of the year, for instance a user 
  during the summer prefers to go to south Europe and in the Winter to the north.
```

### If you had to update the data source in real time, how would your solution change? <a name="bonusquestion2"></a>

```
I would change to use event-driven programming to update the information in real-time.
For example, the service would subscribe to a topic/queue and be triggered after a new
event. One possible solution is using AWS SNS with lambda function (serverless).
```

### What comments would you expect when this goes to a code review? <a name="bonusquestion3"></a>

```
I would expect comments about test improvements. Better naming convention probably. Maybe
changing the json response or the endpoint parameters.
The API behaviour, in this moment the API user HAS TO pass a valid
query string parameter "q" to search insights.
```
