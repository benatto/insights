DROP TABLE IF EXISTS selections;
DROP TABLE IF EXISTS clicks;

CREATE TABLE selections(
    id serial PRIMARY KEY,
    amenity_id NUMERIC NOT NULL,
    user_id NUMERIC NOT NULL,
    quantity NUMERIC NOT NULL,
    timestamp NUMERIC
);

CREATE TABLE clicks(
    id serial PRIMARY KEY,
    user_id NUMERIC NOT NULL,
    hotel_id NUMERIC NOT NULL,
    hotel_region VARCHAR(255),
    quantity NUMERIC NOT NULL,
    timestamp NUMERIC
);

ALTER TABLE selections
    ADD CONSTRAINT selections_uq
    UNIQUE (amenity_id, user_id);

ALTER TABLE clicks
    ADD CONSTRAINT clicks_uq
    UNIQUE (hotel_id, user_id);

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(1, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(2, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(3, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(4, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(5, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(6, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(7, 12, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(8, 11, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO selections(id, amenity_id, user_id, quantity, timestamp)
    VALUES(9, 13, 33, 1, 12344)
    ON CONFLICT (amenity_id, user_id)
    DO UPDATE SET quantity = selections.quantity + 1;

INSERT INTO clicks(id, hotel_id, user_id, hotel_region, quantity, timestamp)
    VALUES(1, 23, 33, 'france', 1, 12344)
    ON CONFLICT (hotel_id, user_id)
    DO UPDATE SET quantity = clicks.quantity + 1;
