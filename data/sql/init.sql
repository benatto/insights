DROP TABLE IF EXISTS selections;
DROP TABLE IF EXISTS clicks;

CREATE TABLE selections(
    id serial PRIMARY KEY,
    amenity_id NUMERIC NOT NULL,
    user_id NUMERIC NOT NULL,
    quantity NUMERIC NOT NULL,
    timestamp NUMERIC
);

CREATE TABLE clicks(
    id serial PRIMARY KEY,
    user_id NUMERIC NOT NULL,
    hotel_id NUMERIC NOT NULL,
    hotel_region VARCHAR(255),
    quantity NUMERIC NOT NULL,
    timestamp NUMERIC
);

ALTER TABLE selections
    ADD CONSTRAINT selections_uq
    UNIQUE (amenity_id, user_id);

ALTER TABLE clicks
    ADD CONSTRAINT clicks_uq
    UNIQUE (hotel_id, user_id);
