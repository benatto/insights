from flask import request
from flask_restful import Resource

from models.click import ClicksModel
from models.selection import SelectionsModel

from resources.constants import DEFAULT_TOP
from resources.utils import is_top_parameter_valid, is_query_parameter_valid


class Insight(Resource):
    @staticmethod
    def _get_top_insights(model, user_id, top):
        insights = model.find_top_insights_by_user_id(user_id, top)
        return [insight.json() for insight in insights]

    def get(self, user_id):
        result = {}

        top = request.args.get("top")
        if not is_top_parameter_valid(top):
            return (
                {"Message": "Invalid request. Please provide a valid top parameter"},
                400,
            )

        qs = request.args.get("q")
        if not is_query_parameter_valid(qs):
            return (
                {"Message": "Invalid request. Please provide a valid query string"},
                400,
            )

        if qs:
            if not top:
                top = DEFAULT_TOP

            if "amenities" in qs:
                amenities = Insight._get_top_insights(SelectionsModel, user_id, top)
                if amenities:
                    result["amenities"] = amenities

            if "hotels" in qs:
                hotels = Insight._get_top_insights(ClicksModel, user_id, top)
                if hotels:
                    result["hotels"] = hotels

        return result, 200
