from resources.constants import VALID_QS_INSIGHTS


def is_top_parameter_valid(top):
    if top:
        try:
            return int(top) > 0
        except (TypeError, ValueError):
            return False
    return True


def is_query_parameter_valid(qs):
    if not qs:
        return False
    else:
        qs_list = qs.split(",")
        for qs in qs_list:
            if qs not in VALID_QS_INSIGHTS:
                return False
    return True
